package logic;

import java.io.*;

/*
 * @author Jan Zabel
 * @version 1.0 vom 05.10.2018 um etwa... zu sp�t
 * Clean Code besagt, das wir (m�glichst) keine Kommentare verwenden sollen, da aber nicht jeder
 * FileWriter und BufferedWriter kennt, beschreibe ich ihr Funktion kurz (Eine Zeile jeweils)
 * Die Methoden besitzen selbstverst�ndliche Namen.
 */

// Methode die Code in Textdatei schreiben soll.
public class WriteFile {
	
	public static void writeIntoFile(String code){
		
		try {
			// Erstellt Datei codes.txt, sofern nicht vorhanden (Daf�r das true statement)
			FileWriter fw = new FileWriter("codes.txt", true);
			// Buffered Writer bietet Methoden f�r einfache bearbeitung der Textdatei an
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(code);
			bw.newLine();
			
			bw.close();
		}
		
		catch (IOException e) {
			System.err.println("Fehler");
		}
		
	}

}
